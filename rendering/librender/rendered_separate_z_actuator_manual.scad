use <../../openscad/libs/upright_z_axis.scad>
use <./rendered_main_body_manual.scad>

separate_z_actuator(no_lug_params(), cable_guides = false, cable_housing = false, rectangular = true);