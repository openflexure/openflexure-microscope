# Assembly Instructions
[![](images/microscopes_less_wide.jpg)](high_res_microscope.md)  
The OpenFlexure Microscope is a 3D printable microscope, with a very precise mechanical translation stage. It is fully motorised, including autofocus, for robotic microscopy, slide scanning and time lapse imaging.  
The microscope is highly [customisable](customisation.md). Find about more about the microscope on the [OpenFlexure website](https://openflexure.org/projects/microscope).

These instructions will take you through how to assemble various configurations. 
There are interactive 3D views of the [RMS](interactive_3d_view_rms.md), [low cost](interactive_3d_view_low_cost.md), and [upright](interactive_3d_view_upright.md) versions of the microscope.

## Microscope Configurations:

#### [High-resolution motorised microscope](high_res_microscope.md)
[![](renders/complete_microscope_rms1.png)](high_res_microscope.md)  
This configuration of the microscope uses a traditional microscope objective for highest image quality.

#### [Motorised microscope with low-cost optics](low_cost_microscope.md)
[![](renders/complete_microscope_low_cost1.png)](low_cost_microscope.md)  
This configuration of the microscope uses the original lens from a Raspberry Pi camera module.

#### [Upright microscope](upright_microscope.md)
[![](renders/complete_microscope_upright1.png)](upright_microscope.md)  
This configuration of the microscope has the objective above the sample rather than below.

#### [Manual microscope](manual_microscope.md)
[![](renders/complete_microscope_low_cost_manual1.png)](manual_microscope.md)  
This configuration of the microscope has thumbwheels for manual motion control, instead of using motors. It does not give access to robotic microscopy, so does not take advantage of many of the key features of the motorised microscope like autofocus. However when built with a USB camera (see the [optics options](customisations/optics_options.md)) it is a compact and very low-cost digital manual microscope.

## Customising your microscope

The OpenFlexure Microscope is designed to be customisable. There are a number of customisation options already available as discussed on our [customisations and alternatives page](customisation.md).

If you want to get more hands on with customisation all source files are available. You can either [download a zip of the source files for this release](source.zip) or visit our [GitLab page](https://gitlab.com/openflexure/openflexure-microscope/) to see our ongoing development. In preparation for the release of v7, we also save a [hash file](models/dependency_hashes.yaml.gz) that will allow us to determine which STL files have changed between revisions.
