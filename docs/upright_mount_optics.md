# Mount the optics


{{BOM}}

[M3 nut]: parts/mechanical.yml#Nut_M3_SS "{cat:mech}"
[M3 stainless steel washers]: parts/mechanical.yml#Washer_M3_SS "{cat:mech}"
[M3x10 cap head screws]: parts/mechanical.yml#CapScrew_M3x10mm_SS "{cat:mech}"
[2.5mm Ball-end Allen key]: parts/tools/2.5mmBallEndAllenKey.md

## Mount the z-actuator mount onto the main body {pagestep}
![](renders/mount_illumination_upright1.png)
![](renders/mount_illumination_upright2.png)

* Place the [Upright z-actuator mount](fromstep){qty:1, cat:printedpart} on top of the main body, triangular face of each touching one another
* Place [M3x10 cap head screws]{qty: 3} and [M3 washers][M3 stainless steel washers]{qty: 3} into the two externally showing holes at the front of the main body and a third screw through the top of the vertical bored hole
* Secure in place with using a [2.5mm Ball-end Allen key]{qty:1} 

## Mount the separate z-axis {pagestep}
![](renders/mount_illumination_upright3.png)
![](renders/mount_illumination_upright4.png)

* Place a [M3 nut]{qty:1, cat:mech} into one of the four nut traps in the top of the z-actuator mount
* Put an [M3x10 cap head screw][extra M3x10 cap screw](parts/mechanical.yml#CapScrew_M3x10mm_SS){qty: 1, cat:tool} into the hole above the nut and tighten the nuts into the nut traps using a 2.5mm Ball-end Allen key
* Remove the screw and repeat the process for to embed [M3 nuts][M3 nut]{qty:3, cat:mech} in the other three nut traps
* Place the [complete separate z-actuator](fromstep){qty:1, cat:subassembly} upside-down onto the z-actuator mount 
* While holding these together, place 4 [M3x10 cap head screws](parts/mechanical.yml#CapScrew_M3x10mm_SS){qty: 4, cat:mech} with [M3 washers][M3 stainless steel washers]{qty: 4} into the 4 bored holes in the separate z-actuator and screw tightly into place 

## Place the optics module {pagestep}
![](renders/mount_upright_optics1.png)
![](renders/mount_upright_optics2.png)

* Get a [2.5mm Ball-end Allen key]{qty:1, cat:tool, note: " Must be a ball-ended key"} ready
* Take the [complete optics module](fromstep){qty:1, cat:subassembly} and holding upside down beside the separate z-actuator
* Insert the exposed mounting screw on the optics module through the keyhole on the separate z-actuator

## Secure the optics module {pagestep}
![](renders/mount_upright_optics3.png)
![](renders/mount_upright_optics4.png)
![](renders/mount_upright_optics5.png)

* Insert the [2.5mm Ball-end Allen key]{qty:1, cat:tool, note: " Must be a ball-ended key"}  through the teardrop shaped hole on the back of the separate z-actuator, until it engages with the mounting screw
* Slide optics module down the keyhole to the bottom, while keeping the Allen key engaged with the screw 
* Tighten the screw with the Allen key to lock the optics in place

## Add the foot cap {pagestep}
![](renders/mount_upright_optics6.png)
![](renders/mount_upright_optics7.png)

* Take the [foot cap](fromstep){qty:1, cat:printedpart} and locate it over the exposed foot at the top of the microscope
* Push the cap in to place
