# Attach the sample clips


{{BOM}}

## Prepare the sample clips {pagestep}

![](renders/prep_sample_clips_1.png)
![](renders/prep_sample_clips_2.png)

* Take one [sample clip][Sample clips](fromstep){qty:1}.
* Insert an [M3x10 cap screw][M3x10 cap head screws](parts/mechanical.yml#CapScrew_M3x10mm_SS){qty: 2, cat:mech} through the hole at the end of the clip.
* Repeat for the other clip

## Attaching the sample clips {pagestep}

![](renders/mount_sample_clips_{{var_optics, default:rms}}{{var_body, default:}}1.png)
![](renders/mount_sample_clips_{{var_optics, default:rms}}{{var_body, default:}}2.png)
![](renders/mount_sample_clips_{{var_optics, default:rms}}{{var_body, default:}}3.png)

* Insert a [2.5mm Ball-end Allen key](parts/tools/2.5mmBallEndAllenKey.md){cat:tool, qty:1} through the hole in the curved part of a clip, and into the screw.
* Screw the clip down onto the microscope stage.
* Repeat for the other clip.
