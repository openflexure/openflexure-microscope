# Prepare the separate z-actuator

{{BOM}}

[utility knife]: parts/tools/utility-knife.md "{cat:tool}"
[precision wire cutters]: parts/tools/precision-wire-cutters.md "{cat:tool}"

## Removing brim and supports {pagestep}

The [separate z-actuator](fromstep){cat: PrintedPart, qty:1} has some custom supports and a custom brim to remove. These are highlighted in red in the following images.

![](renders/brim_and_ties_separate_z1.png)
![](renders/brim_and_ties_separate_z2.png)

* Remove the brim with [utility knife]{qty:1} and [precision wire cutters]{qty:1}
* Cut the ties inside actuator column (2 total) with the precision wire cutters

[prepared separate z actuator]{output, qty:1, hidden}