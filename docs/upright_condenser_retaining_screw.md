[M3 nut]: parts/mechanical.yml#Nut_M3_SS "{cat:mech}"
[M3x10 cap head screws]: parts/mechanical.yml#CapScrew_M3x10mm_SS "{cat:mech}"

## Add the condenser mounting screw {pagestep}

* Take an [M3 nut]{qty:1} and push it into the nut trap in the condenser platform from the top
* Take an [M3x10 cap head screw][M3x10 cap head screws]{qty:1} and screw it into the nut, only screw a couple of turns about **5mm of thread should still be visible at this stage** 
