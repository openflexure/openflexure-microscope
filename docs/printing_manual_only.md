* [Manual main body]{output,qty:1}: [main_body_manual.stl](models/main_body_manual.stl){previewpage} - The smart brim may require [custom print settings].
* [Microscope simple post stand][microscope stand]{output, qty:1}: [simple_post_stand.stl](models/simple_post_stand.stl){previewpage}. As a more stable option you could use the [microscope_stand_no_pi.stl](models/microscope_stand_no_pi.stl){previewpage} instead
* 3 [thumbwheels]{output,qty:3}: [thumbwheels.stl](models/thumbwheels.stl){previewpage}
