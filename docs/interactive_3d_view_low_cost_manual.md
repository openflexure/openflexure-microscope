# Interactive 3D view of the OpenFlexure Microscope

Below is an interactive 3D view of the assembled microscope. This page shows the simplest OpenFlexure microscope, a manual microscope using low cost optics using a Raspberry Pi camera module, and employing that camera module's lens as the microscope objective.

![A render of the completed microscope](gltf/complete_microscope_manual.glb)
