# Electronics drawers alternative electronics versions

As standard the OpenFlexure microscope uses a Raspberry Pi v4 and a Sangaboard v0.5. If you have a different Raspberry Pi or Sangaboard you will need a different electronics drawer.

>! **Note on Sangaboard v0.5**
>!
>! The Sangaboard v0.5 from our preferred supplier has 11mm tall header connectors. If you purchase a board from another supplier and they use standard 8.5mm headers you will need to print a different electronics drawer.

## Drawer options:

|  | Raspberry Pi v3 | Raspberry Pi v4 |
|--|--|--|
|**Sangaboard v0.3**| [electronics_drawer-pi3_sanga_v0.3.stl](../models/electronics_drawer-pi3_sanga_v0.3.stl){previewpage} | [electronics_drawer-pi4_sanga_v0.3.stl](../models/electronics_drawer-pi4_sanga_v0.3.stl){previewpage} |
|**Sangaboard v0.4**| [electronics_drawer-pi3_sanga_stack_8.5mm.stl](../models/electronics_drawer-pi3_sanga_stack_8.5mm.stl){previewpage} | [electronics_drawer-pi4_sanga_stack_8.5mm.stl](../models/electronics_drawer-pi4_sanga_stack_8.5mm.stl){previewpage} |
|**Sangaboard v0.5** | [electronics_drawer-pi3_sanga_stack_11mm.stl](../models/electronics_drawer-pi3_sanga_stack_11mm.stl){previewpage} | **[electronics_drawer-pi4_sanga_stack_11mm.stl](../models/electronics_drawer-pi4_sanga_stack_11mm.stl){previewpage}** | 
|**Sangaboard v0.5 (with 8.5mm headers)** | [electronics_drawer-pi3_sanga_stack_8.5mm.stl](../models/electronics_drawer-pi3_sanga_stack_8.5mm.stl){previewpage} | [electronics_drawer-pi4_sanga_stack_8.5mm.stl](../models/electronics_drawer-pi4_sanga_stack_8.5mm.stl){previewpage} |
|**Sangaboard workaround** | [electronics_drawer-pi3_sanga_stack_11mm.stl](../models/electronics_drawer-pi3_sanga_stack_11mm.stl){previewpage} | **[electronics_drawer-pi4_sanga_stack_11mm.stl](../models/electronics_drawer-pi4_sanga_stack_11mm.stl){previewpage}** |

---

>i The bolded **[electronics_drawer-pi4_sanga_stack_11mm.stl](../models/electronics_drawer-pi4_sanga_stack_11mm.stl){previewpage}** is the standard electronics drawer