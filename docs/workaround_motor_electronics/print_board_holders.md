# Print the holders for the motor control electronics

This page describes the parts to print if you are using three separate motor driver boards and an Arduino Nano microcontroller to drive your motors.

{{BOM}}

[PLA filament]: ../parts/materials/pla_filament.md "{cat:material}"
[RepRap-style printer]: ../parts/tools/rep-rap.md "{cat:tool}"

## Print the converter plate and board gripper {pagestep}

Using a [RepRap-style printer]{qty:1}, print the following parts using [PLA filament]{qty: 50 grams}.

* [nano_converter_plate-pi4.stl](../models/nano_converter_plate-pi4.stl){previewpage}  
* [nano_converter_plate_gripper.stl](../models/nano_converter_plate_gripper.stl){previewpage} (print in the side-on orientation as in the STL to avoid unsupported overhang)

As long as you are using a Raspberry Pi v4 you can use the standard electronics drawer.

>! **Attention Raspberry Pi version 3 users**
>!
>! For a Raspberry Pi version 3, you will need a different electronics drawer and adapter plate  
>! 
>! * [electronics_drawer-pi3_sanga_stack_11mm.stl](../models/electronics_drawer-pi3_sanga_stack_11mm.stl){previewpage}
>! * [nano_converter_plate-pi3.stl](../models/nano_converter_plate-pi3.stl){previewpage}
>!
>! You also need the same [nano_converter_plate_gripper.stl](../models/nano_converter_plate_gripper.stl){previewpage}

[nano converter plate]{output, qty:1, hidden}
[nano converter plate gripper]{output, qty:1, hidden}

