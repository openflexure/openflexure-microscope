>i These instructions guide you through assembling the manual microscope with a Raspberry Pi camera v2 in a basic optics module.  
>i For the most compact and low cost system, use this assembly method with a Logitech C270 webcam or Arducam B0196 webcam.   
>i
>i STLs can be found in the list of basic optics modules on the [optics customisations](customisations/optics_options.md) page.