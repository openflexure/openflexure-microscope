
## Repeat the process again for the separate z-axis  {pagestep}
![separate z actuator assembled](renders/separate_z_actuator_assembled.png)

* Follow the same procedure to install the gear, foot and Viton band for the [separate Z actuator][prepared separate z actuator](fromstep){qty:1, cat:subassembly}
* Make sure that you have put a drop of light oil on the thread
* Make sure that the foot is seated 
* Make sure that this axis has two washers as well

[complete separate z-actuator]{output, qty:1, hidden}

