# High-resolution microscope

This is the standard research version of the OpenFlexure Microscope. It supports Standard RMS threaded objectives. It is a fully functioning motorised laboratory grade microscope. For more information on the microscope and its performance [see our paper in Optics Express](https://doi.org/10.1364/BOE.385729).

If you use this microscope for research please consider citing this paper.

>? For known bugs, building tips, and advice, please use our [Forum](https://openflexure.discourse.group/). If you find any problems with the build, please let us know on [GitLab](https://gitlab.com/openflexure/openflexure-microscope/-/issues) or on the Forum.

Before you start building the microscope you will need to source all of the components listed in the [bill of materials]{bom}.


The assembly is broken up into several steps:

1. [.](test_your_printer.md){step}
1. [.](printing.md){step, var_type: high_res}
1. [.](prepare_main_body.md){step}
1. [.](prepare_stand.md){step}
1. [.](actuator_assembly.md){step, var_n_actuators:3}
1. [.](high_res_optics_module.md){step}
1. [.](mount_optics_and_microscope.md){step, var_optics: rms}
1. [.](illumination.md){step, var_type: high_res}
1. [.](mount_illumination.md){step, var_type: high_res}
1. [.](motors.md){step, var_optics: rms}
1. [.](attach_clips.md){step, var_optics: rms}
1. [.](prepare_pi_and_sangaboard.md){step}
1. [.](wiring.md){step, var_optics: rms}
1. [.](finished.md){step, var_optics: rms}

![A render of the completed high resolution microscope](renders/complete_microscope_rms1.png)

There is also an [interactive 3D view](interactive_3d_view_rms.md) of the finished microscope.