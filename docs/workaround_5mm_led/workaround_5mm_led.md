# Using a 5mm LED instead of the illumination PCB

The condenser assembly is designed for use with an illumination PCB, containing a white LED in an easy to mount package that is driven by a constant current circuit on a Sangaboard v0.5.  It is still possible to use a 5mm LED, mounted onto the lid of the condenser and connected to a regular 5V power supply. This page contains instructions for preparing and mounting the LED.  It requires, in addition to the parts needed for the regular condenser, two extra self tapping screws, the 5mm LED and resistor, and the condenser led holder.

{{BOM}}

* [Solder the LED](./solder_led.md){step}
* [Print the LED holder](./print_led_holder.md){step}
* [Mount the LED into the condenser lid](./mount_5mm_led.md){step}
* [Power the LED](./power_workaround_led.md){step}