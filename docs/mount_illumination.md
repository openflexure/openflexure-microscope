# Mount the illumination

{{BOM}}

[M3 nut]: parts/mechanical.yml#Nut_M3_SS "{cat:mech}"
[M3 stainless steel washers]: parts/mechanical.yml#Washer_M3_SS "{cat:mech}"
[M3x10 cap head screws]: parts/mechanical.yml#CapScrew_M3x10mm_SS "{cat:mech}"
[2.5mm Ball-end Allen key]: parts/tools/2.5mmBallEndAllenKey.md "{cat:tool}"

## Mount the dovetail {pagestep}

![](renders/mount_illumination_{{var_optics, default:rms}}{{var_body, default:}}1.png)
![](renders/mount_illumination_{{var_optics, default:rms}}{{var_body, default:}}2.png)


* Place the [illumination dovetail][Illumination dovetail](fromstep){qty:1, cat:printedpart} onto the stage above the z-actuator of the main body.
* Secure in place with two [M3x10 cap head screws]{qty:2} and 2 [M3 Washers][M3 stainless steel washers]{qty: 2} (using [2.5mm Ball-end Allen key]{qty:1})

## Insert the illumination wiring {pagestep}

![](renders/mount_illumination_{{var_optics, default:rms}}{{var_body, default:}}3.png)

* Take the [assembled illumination module](fromstep){qty: 1, cat:subassembly}
* Pass the illumination cable from the top to the bottom of the cable guide in the illumination dovetail.
* Pass the illumination cable from the top to the bottom of the cable guide in the main body, between the Z gear and the Y gear.

## Mount the condenser arm {pagestep}

![](renders/mount_illumination_{{var_optics, default:rms}}{{var_body, default:}}4.png)
![](renders/mount_illumination_{{var_optics, default:rms}}{{var_body, default:}}5.png)
![](renders/mount_illumination_{{var_optics, default:rms}}{{var_body, default:}}6.png)

* Slide the condenser arm into the illumination dovetail until it is approximately flush with the top
* Tighten the thumbscrew by hand to lock the arm in place.
* Do not worry about the exact position, this will be adjusted on first use.

