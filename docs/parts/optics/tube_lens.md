---
PartData:
  Specs:
    Focal Length: 50 mm
    Outer Diameter: 12.7 mm
    Material: Glass
    Lens Type: Cemented achromatic doublet
    Antireflection Coating: For visible wavelengths
  Suppliers:
    ThorLabs:
      Link: https://www.thorlabs.com/thorproduct.cfm?partnumber=AC127-050-A
      PartNo: AC127-050-A
    AliExpress:
      Link: https://www.aliexpress.com/wholesale?SearchText=50mm+achromatic+lens 
      PartNo: Search for 50mm achromatic lens
    Surplus Shed:
      Link: https://www.surplusshed.com/search_lenses.php?type=ACH&diameter_to=12.5&diameter_from=13&focal_length_to=48&focal_length_from=52&sort=diameter&sortby=+asc
      PartNo: Search for the specifications given below
---

# 12.7 mm acromatic doublet lens (50 mm focal length)

This lens is used to between the microscope objective and the camera sensor to modify the field of view. It is important to buy an acromatic doublet lens, so that is corrected for a range of wavelengths of light.  The ThorLabs part linked to above is achromatic.

ThorLabs is a scientific supplier, used to dealing with high-quality, low-quantity orders.  This means they are more expensive than many consumer electronics products, but they are very consistent and reliable.

It is also possible to source these lenses from other suppliers at much lower cost, for example [AliExpress].  Unfortunately AliExpress is a marketplace rather than a single supplier, so it is not possible to include a persistent link.  The important things to look for are a focal length of 50mm and an outer diameter of 12.7mm (between 12.5mm and 13mm should be OK, there is a little tolerance in the printed gripper).  The lens should be described as "achromatic", "achromat", "cemented", or "doublet".  Usually there will be an antireflective coating for visible wavelengths.  I would expect to pay between £10 and £15 plus shipping (and applicable tax) per lens.

It may be worth looking in to other local suppliers - for example some users have had success with [Surplus Shed](https://www.surplusshed.com/).  While the optics module is designed for use with a 50mm focal length, varying this slightly will only result in slight aberrations, and so a focal length between 48mm and 52mm should be fine for most uses.

It is also a very good idea to check the [forum](https://openflexure.discourse.group/) for suggestions, as there may be up-to-date links to specific products on AliExpress or eBay, as well as other microscope builders willing to share spare parts or put together group orders.  There was a lot of activity in the [Sourcing non-printed parts](https://openflexure.discourse.group/t/sourcing-non-printed-parts/61) thread, for example.

[AliExpress]: https://www.aliexpress.com/wholesale?SearchText=50mm+achromatic+lens "Search for 50mm achromatic lens on AliExpress"